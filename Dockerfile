FROM debian:11-slim

ARG POSTGRES_VERSION
ENV POSTGRES_VERSION=${POSTGRES_VERSION:-14}

ARG PGWATCH2_VERSION
ENV PGWATCH2_VERSION=${PGWATCH2_VERSION:-1.11.0}
ARG GRAFANA_VERSION
ENV GRAFANA_VERSION=${GRAFANA_VERSION:-10.0.2}

# More info at https://pgwatch2.readthedocs.io/en/latest/custom_installation.html#yaml-based-setup

# Install Postgres or use any available existing instance - v11+ required for the metrics DB.
RUN apt-get clean && rm -rf /var/lib/apt/lists/partial \
   && apt-get update -o Acquire::CompressionTypes::Order::=gz \
   && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y gnupg openssh-client ca-certificates wget curl vim-tiny \
   # PostgreSQL
   && echo "deb http://apt.postgresql.org/pub/repos/apt bullseye-pgdg main" > /etc/apt/sources.list.d/pgdg.list \
   && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
   && apt-get update -o Acquire::CompressionTypes::Order::=gz \
   && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y postgresql-${POSTGRES_VERSION} \
   && sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && locale-gen \
   && pg_dropcluster ${POSTGRES_VERSION} main; pg_createcluster --locale en_US.UTF-8 ${POSTGRES_VERSION} main -- --data-checksums --auth-local=peer --auth-host=scram-sha-256 \
   && echo "include = 'pgwatch_postgresql.conf'" >> /etc/postgresql/${POSTGRES_VERSION}/main/postgresql.conf \
   # TimescaleDB
   && echo "deb https://packagecloud.io/timescale/timescaledb/debian/ bullseye main" > /etc/apt/sources.list.d/timescaledb.list \
   && wget --quiet -O - https://packagecloud.io/timescale/timescaledb/gpgkey | apt-key add - \
   && apt-get update -o Acquire::CompressionTypes::Order::=gz \
   && DEBIAN_FRONTEND=noninteractive apt-get install -y timescaledb-2-postgresql-${POSTGRES_VERSION} \
   # pg_stat_kcache
   && DEBIAN_FRONTEND=noninteractive apt-get install postgresql-${POSTGRES_VERSION}-pg-stat-kcache \
   # pg_wait_sampling
   && DEBIAN_FRONTEND=noninteractive apt-get install postgresql-${POSTGRES_VERSION}-pg-wait-sampling \
   # supervisor
   && DEBIAN_FRONTEND=noninteractive apt-get install -y supervisor

# Install pgwatch2 from pre-built packages.
RUN wget --quiet -O pgwatch2.deb https://github.com/cybertec-postgresql/pgwatch2/releases/download/v${PGWATCH2_VERSION}/pgwatch2_${PGWATCH2_VERSION}_linux_64-bit.deb \
   && dpkg -i pgwatch2.deb && rm pgwatch2.deb \
   # delete pre-installed grafana dashboards
   && rm -rf /etc/pgwatch2/grafana-dashboards/* \
   # delete pre-installed metrics
   && rm -rf /etc/pgwatch2/metrics/*

ADD bootstrap /etc/pgwatch2/bootstrap
ADD grafana-dashboards /etc/pgwatch2/grafana-dashboards
ADD metrics /etc/pgwatch2/metrics
COPY docker-launcher.sh /usr/bin/
COPY postgresql.conf /etc/postgresql/${POSTGRES_VERSION}/main/pgwatch_postgresql.conf
COPY pg_hba.conf /etc/postgresql/${POSTGRES_VERSION}/main/pg_hba.conf

# Install and configure Grafana.
RUN DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y libfontconfig1 \
   && wget -q -O grafana.deb https://dl.grafana.com/oss/release/grafana_${GRAFANA_VERSION}_amd64.deb \
   && dpkg -i grafana.deb && rm grafana.deb \
   && cp /etc/pgwatch2/bootstrap/grafana_custom_config.ini /etc/grafana/grafana.ini

# Clean up
RUN apt-get autoremove -y --purge gnupg wget \
   && apt-get clean -y autoclean \
   && rm -rf /var/lib/apt/lists/*

# Set up supervisord [https://docs.docker.com/engine/admin/using_supervisord/]
COPY supervisord.conf /etc/supervisor/supervisord.conf
RUN sed -i "s/POSTGRES_VERSION/${POSTGRES_VERSION}/g" /etc/supervisor/supervisord.conf

ENV PW2_DATASTORE postgres
ENV PW2_PG_METRIC_STORE_CONN_STR postgresql://pgwatch2:pgwatch2admin@localhost:5432/pgwatch2_metrics
ENV PW2_PG_SCHEMA_TYPE timescale
ENV PW2_AES_GCM_KEYPHRASE_FILE /etc/pgwatch2/persistent-config/default-password-encryption-key.txt

# Gatherer healthcheck port / metric statistics (JSON)
EXPOSE 8081
# Postgres DB holding the pgwatch2 metrics
EXPOSE 5432
# Grafana UI
EXPOSE 3000

### Volumes for easier updating to newer to newer pgwatch2 containers
### NB! Backwards compatibility is not 100% guaranteed so a backup
### using traditional means is still recommended before updating - see "Updating to a newer Docker version" in the documentation https://pgwatch2.readthedocs.io

VOLUME /etc/pgwatch2/persistent-config
VOLUME /var/lib/postgresql
VOLUME /var/lib/grafana

CMD ["/usr/bin/docker-launcher.sh"]
