SELECT /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  relname::text as tag_table_name,
  indexrelname::text as tag_index_name,
  idx_scan as index_scans
FROM pg_stat_user_indexes
  JOIN pg_index
  USING (indexrelid)
WHERE indisunique IS false;
