WITH q_data AS ( /* pgwatch2_generated */
    SELECT
        (regexp_replace(md5(query::varchar(1000)), E'\\D', '', 'g'))::varchar(10)::text as tag_queryid,
        max(query::varchar(8000)) AS query,
        array_to_string(array_agg(DISTINCT quote_ident(pg_get_userbyid(userid))), ',') AS users,
        sum(s.calls)::int8 AS calls,
        round(sum(s.total_time)::numeric, 3)::double precision AS total_time,
        sum(s.rows)::int8 as rows,
        sum(shared_blks_hit)::int8 AS shared_blks_hit,
        sum(shared_blks_read)::int8 AS shared_blks_read,
        sum(shared_blks_written)::int8 AS shared_blks_written,
        sum(shared_blks_dirtied)::int8 AS shared_blks_dirtied,
        sum(shared_blks_hit)::int8 * current_setting('block_size')::int as shared_blks_hit_bytes,
        sum(shared_blks_read)::int8 * current_setting('block_size')::int as shared_blks_read_bytes,
        sum(shared_blks_written)::int8 * current_setting('block_size')::int as shared_blks_written_bytes,
        sum(shared_blks_dirtied)::int8 * current_setting('block_size')::int as shared_blks_dirtied_bytes,
        sum(temp_blks_read)::int8 AS temp_blks_read,
        sum(temp_blks_written)::int8 AS temp_blks_written,
        round(sum(blk_read_time)::numeric, 3)::double precision AS blk_read_time,
        round(sum(blk_write_time)::numeric, 3)::double precision AS blk_write_time
    FROM
        pg_stat_statements s
    WHERE
        dbid = (
            SELECT
                oid
            FROM
                pg_database
            WHERE
                datname = current_database())
            AND NOT upper(s.query::varchar(50))
            LIKE ANY (ARRAY['DEALLOCATE%',
                'SET %',
                'RESET %',
                'BEGIN%',
                'BEGIN;',
                'COMMIT%',
                'END%',
                'ROLLBACK%',
                'SHOW%',
                'DISCARD ALL'])
        GROUP BY
            tag_queryid
)
SELECT (EXTRACT(epoch FROM now()) * 1e9)::int8 AS epoch_ns,
       b.tag_queryid,
       b.users,
       b.calls,
       b.total_time,
       b.rows,
       b.shared_blks_hit,
       b.shared_blks_read,
       b.shared_blks_written,
       b.shared_blks_dirtied,
       b.shared_blks_hit_bytes,
       b.shared_blks_read_bytes,
       b.shared_blks_written_bytes,
       b.shared_blks_dirtied_bytes,
       b.temp_blks_read,
       b.temp_blks_written,
       b.blk_read_time,
       b.blk_write_time,
       ltrim(regexp_replace(b.query, E'[ \\t\\n\\r]+', ' ', 'g')) tag_query
FROM (
    SELECT
        *
    FROM (
        SELECT
            *
        FROM
            q_data
        WHERE
            total_time > 0
        ORDER BY
            total_time DESC
        LIMIT 100) a
UNION
SELECT
    *
FROM (
    SELECT
        *
    FROM
        q_data
    ORDER BY
        calls DESC
    LIMIT 100) a
UNION
SELECT
    *
FROM (
    SELECT
        *
    FROM
        q_data
    WHERE
        shared_blks_read > 0
    ORDER BY
        shared_blks_read DESC
    LIMIT 100) a
UNION
SELECT
    *
FROM (
    SELECT
        *
    FROM
        q_data
    WHERE
        shared_blks_hit > 0
    ORDER BY
        shared_blks_hit DESC
    LIMIT 100) a
UNION
SELECT
    *
FROM (
    SELECT
        *
    FROM
        q_data
    WHERE
        shared_blks_written > 0
    ORDER BY
        shared_blks_written DESC
    LIMIT 100) a
UNION
SELECT
    *
FROM (
    SELECT
        *
    FROM
        q_data
    WHERE
        temp_blks_read > 0
    ORDER BY
        temp_blks_read DESC
    LIMIT 100) a
UNION
SELECT
    *
FROM (
    SELECT
        *
    FROM
        q_data
    WHERE
        temp_blks_written > 0
    ORDER BY
        temp_blks_written DESC
    LIMIT 100) a) b;
