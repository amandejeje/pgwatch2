SELECT /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  p.queryid as tag_queryid,
  p.event_type as tag_wait_type, 
  p.event as tag_wait_event,
sum(p.count)::int8 as of_events
FROM pg_wait_sampling_profile p
LEFT JOIN pg_stat_statements s ON p.queryid = s.queryid
WHERE s.dbid = (select oid from pg_database where datname = current_database())
GROUP BY p.queryid, p.event_type, p.event;
