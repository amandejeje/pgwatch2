SELECT /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  1 as result
FROM ( select pg_stat_statements_reset()) as pg_stat_statements_reset;
