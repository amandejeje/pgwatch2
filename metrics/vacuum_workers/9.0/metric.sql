with pgsa as ( /* pgwatch2_generated */
  select * from pg_stat_activity where query ilike '%vacuum%' and pid <> pg_backend_pid() and datname = current_database()
)
select
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  (select count(*) from pgsa where state = 'active' and query like 'autovacuum%') AS av_workers,
  (select count(*) from pgsa where state = 'active' and query ilike 'vacuum%') AS v_manual,
  (select extract(epoch from (now() - xact_start))::int8 from pgsa where xact_start is not null order by xact_start limit 1) as vac_maxtime;
