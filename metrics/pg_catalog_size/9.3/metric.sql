SELECT /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  (sum(pg_total_relation_size(relname::regclass)) / 1024 / 1024)::int as pg_catalog_size
FROM 
  pg_stat_sys_tables 
WHERE 
  schemaname = 'pg_catalog';
